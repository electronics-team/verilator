#!/usr/bin/make -f
# -*- makefile -*-

#export DH_VERBOSE = 1

# Switch on the hardening
export DEB_BUILD_MAINT_OPTIONS += hardening=+all
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/buildflags.mk
include /usr/share/dpkg/pkg-info.mk

ifneq (,$(filter $(DEB_HOST_ARCH), mipsel))
  export DEB_CPPFLAGS_MAINT_APPEND += -g1
endif

CLEAN_OUT_DATA := \
config.log \
configure \
docs/_build \
src/config_rev.h \
verilator-config.cmake \
verilator-config-version.cmake \
verilator.pc \
$(NULL)

%:
	dh $@

override_dh_clean:
	dh_clean
	# Reset src/config_rev into the original state if needed.
	if [ -f debian/config_rev.save ]; then \
	    cp -f debian/config_rev.save src/config_rev ;\
	fi
	rm -rf $(CLEAN_OUT_DATA)

override_dh_autoreconf:
	# Work around we need to keep the original files for idempotency in case
	# we run dh_clean. The file isn't generated from "§SOMETHING".
	if [ ! -f debian/config_rev.save ]; then \
	    cp src/config_rev debian/config_rev.save ;\
	fi
	# We don't build on top of a working git tree and the build system
	# can't find a git revision. Adding a Debian version to the final
	# string instead.
	sed -i 's/UNKNOWN_REV/(Debian $(DEB_VERSION))/g' src/config_rev
	autoconf

override_dh_auto_build:
	dh_auto_build
	##########################################
	# Build sphinx based HTML documentation. #
	##########################################
	cd docs && make html
	#########################################
	# Build sphinx based PDF documentation. #
	#########################################
	cd docs && make pdf

override_dh_compress:
	dh_compress --exclude=examples --exclude=verilator.pdf

override_dh_fixperms:
	dh_fixperms
	# Fixing file permissions for Python based files in /usr/share/verilator/examples.
	for file in `find $(CURDIR)/debian/verilator/usr/share/verilator/examples -type f`; do \
		FOUND_PYTHON=$$(head -n1 $$file | grep python); \
		if [ "$$FOUND_PYTHON" != "" ]; then \
			chmod +x $$file; \
		else \
			chmod -x $$file; \
		fi; \
	done

# armel, mips{,64}el will break due libatomic issues, can be ignored for dh_auto_test.
override_dh_auto_test:
ifeq (,$(filter armel mipsel mips64el,$(DEB_BUILD_ARCH)))
	dh_auto_test
else
	@echo "#########################################"
	@echo "# Skipping calling dh_auto_test on $(DEB_BUILD_ARCH)"
	@echo "#########################################"
endif

# Ignore dwz, it's failing and does gain nothing.
override_dh_dwz:
